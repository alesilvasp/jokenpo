const bigContainer = document.getElementById('bigContainer')
const container = document.getElementById('container');
const rock = document.getElementById('rock');
const paper = document.getElementById('paper');
const scissors = document.getElementById('scissors');
const combat = document.getElementById('combat');
const spanMsg = document.getElementById('span_msg');
const reset = document.getElementById('reset');

// função onde gera escolha aleatória do computador
function computersChoice() {
    let randomChoice = Math.floor(1 + 3 * Math.random());
    let computerChoice = 0;
    if (randomChoice === 1) {
        computerChoice = rock;
    }
    else if (randomChoice === 2) {
        computerChoice = paper;
    }
    else {
        computerChoice = scissors;
    }
    return computerChoice;
}

// função onde o usuário escolhe uma das três opções e mostra a escolha do computador e no final o vencedor;
function usersChoice(event) {

    container.style.width = '45%';
    container.style.justifyContent = 'space-between';
    container.style.margin = '0 auto';
    // colocar condição if event.target 
    if (event.target === rock) {
        paper.remove('paper');
        scissors.remove('scissors');
    } else if (event.target == paper) {
        paper.style.left = '0';
        rock.remove('rock');
        scissors.remove('scissors');
    } else {
        scissors.style.left = '0';
        rock.remove('rock');
        paper.remove('paper');

    }

    // escolha do computador 
    const choiceComputerImg = document.createElement('img')
    choiceComputerImg.classList.add('cpChoiceImg')
    let cpChoice = computersChoice();
    if (cpChoice === rock) {
        choiceComputerImg.src = './img/cp_pedra1.png';
    } else if (cpChoice === paper) {
        choiceComputerImg.src = './img/cp_papel1.png';
    } else {
        choiceComputerImg.src = './img/cp_tesoura1.png';
    }
    container.appendChild(choiceComputerImg);

    // Mostra o vencedor
    const winnerSpan = document.createElement('span');
    spanMsg.innerText = '';
    winnerSpan.innerText = winner(event, cpChoice);
    spanMsg.appendChild(winnerSpan);
}
// Função para descobrir o vencedor
function winner(user, computer) {

    let roundWinner = 0;

    if (user.target === computer) {
        roundWinner = 'Draw';
    } else if (user.target === rock && computer == paper) {
        roundWinner = 'Computer Wins!';
    } else if (user.target === rock && computer == scissors) {
        roundWinner = 'Player Wins!';
    } else if (user.target === paper && computer == scissors) {
        roundWinner = 'Computer Wins!';
    } else if (user.target === paper && computer == rock) {
        roundWinner = 'Player Wins!';
    } else if (user.target === scissors && computer == rock) {
        roundWinner = 'Computer Wins!';
    } else {
        roundWinner = 'Player Wins!';
    }
    return roundWinner;
}

function resetGame() {
    spanMsg.innerText = 'VS';
    container.innerHTML = '';
    container.appendChild(rock);
    container.appendChild(paper);
    container.appendChild(scissors);
    styleDefault();
}

// reset Styles Defalt
function styleDefault() {
    container.style.width = '45%';
    container.style.justifyContent = 'flex-start';
    container.style.margin = '0';
}
container.addEventListener('click', usersChoice);
reset.addEventListener('click', resetGame);
